package org.coderfun.workspace.dict.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import klg.common.dataaccess.entity.OrderEntity_;

@Generated(value="Dali", date="2018-12-12T14:30:40.754+0800")
@StaticMetamodel(WPCodeClass.class)
public class WPCodeClass_ extends OrderEntity_ {
	public static volatile SingularAttribute<WPCodeClass, String> code;
	public static volatile SingularAttribute<WPCodeClass, String> name;
	public static volatile SingularAttribute<WPCodeClass, String> value;
	public static volatile SingularAttribute<WPCodeClass, String> moduleCode;
	public static volatile SingularAttribute<WPCodeClass, String> isSys;
	public static volatile SingularAttribute<WPCodeClass, Long> workspaceId;
}
