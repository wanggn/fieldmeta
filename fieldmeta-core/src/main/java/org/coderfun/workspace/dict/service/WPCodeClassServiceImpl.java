package org.coderfun.workspace.dict.service;

import org.coderfun.workspace.dict.dao.WPCodeClassDAO;
import org.coderfun.workspace.dict.entity.WPCodeClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import klg.common.dataaccess.BaseServiceImpl;

@Service
public class WPCodeClassServiceImpl  extends BaseServiceImpl<WPCodeClass, Long> implements WPCodeClassService{
	@Autowired
	WPCodeClassDAO codeClassDAO;
}
