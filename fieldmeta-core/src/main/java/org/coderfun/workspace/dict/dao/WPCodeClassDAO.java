package org.coderfun.workspace.dict.dao;

import org.coderfun.workspace.dict.entity.WPCodeClass;

import klg.common.dataaccess.BaseRepository;

public interface WPCodeClassDAO extends BaseRepository<WPCodeClass, Long> {

}
