package org.coderfun.workspace.dict.service;

import org.coderfun.workspace.dict.dao.WPCodeItemDAO;
import org.coderfun.workspace.dict.entity.WPCodeItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import klg.common.dataaccess.BaseServiceImpl;

@Service
public class WPCodeItemServiceImpl  extends BaseServiceImpl<WPCodeItem, Long> implements WPCodeItemService{
	@Autowired
	WPCodeItemDAO codeItemDAO;
}
