package org.coderfun.workspace.dict.service;

import org.coderfun.workspace.dict.entity.WPCodeClass;

import klg.common.dataaccess.BaseService;

public interface WPCodeClassService extends BaseService<WPCodeClass, Long>{

}
