package org.coderfun.workspace.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import klg.common.dataaccess.entity.BaseEntity_;

@Generated(value="Dali", date="2018-12-12T20:26:23.538+0800")
@StaticMetamodel(Template.class)
public class Template_ extends BaseEntity_ {
	public static volatile SingularAttribute<Template, String> name;
	public static volatile SingularAttribute<Template, Long> originalCode;
	public static volatile SingularAttribute<Template, String> info;
	public static volatile SingularAttribute<Template, Long> memberId;
	public static volatile SingularAttribute<Template, String> memberEmail;
}
