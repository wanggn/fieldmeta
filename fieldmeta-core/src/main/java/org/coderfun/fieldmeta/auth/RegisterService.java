package org.coderfun.fieldmeta.auth;

public interface RegisterService {
	
	public void register(String username, String email, String password, String captcha);
	public void resetPassword(String email, String password, String captcha);
}
