package org.coderfun.fieldmeta.dao;

import klg.common.dataaccess.BaseRepository;
import org.coderfun.fieldmeta.entity.Module;

public interface ModuleDAO extends BaseRepository<Module, Long> {

}
