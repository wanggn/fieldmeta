package org.coderfun.fieldmeta.dao;

import klg.common.dataaccess.BaseRepository;
import org.coderfun.fieldmeta.entity.Tablemeta;

public interface TablemetaDAO extends BaseRepository<Tablemeta, Long> {

}
