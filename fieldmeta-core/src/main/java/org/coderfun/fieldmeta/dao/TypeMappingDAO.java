package org.coderfun.fieldmeta.dao;

import klg.common.dataaccess.BaseRepository;
import org.coderfun.fieldmeta.entity.TypeMapping;

public interface TypeMappingDAO extends BaseRepository<TypeMapping, Long> {

}
