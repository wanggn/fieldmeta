package org.coderfun.fieldmeta.service;

import klg.common.dataaccess.BaseService;
import org.coderfun.fieldmeta.entity.TypeMapping;

public interface TypeMappingService extends BaseService<TypeMapping, Long>{

}
