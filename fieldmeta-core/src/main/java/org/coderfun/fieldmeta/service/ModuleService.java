package org.coderfun.fieldmeta.service;

import klg.common.dataaccess.BaseService;

import java.util.List;

import org.coderfun.fieldmeta.entity.Module;

public interface ModuleService extends BaseService<Module, Long>{

	public List<Module> getListByDefalutProject();
	
}
