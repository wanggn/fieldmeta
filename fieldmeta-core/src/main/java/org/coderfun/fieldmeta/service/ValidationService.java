package org.coderfun.fieldmeta.service;

import klg.common.dataaccess.BaseService;
import org.coderfun.fieldmeta.entity.Validation;

public interface ValidationService extends BaseService<Validation, Long>{

}
