package org.coderfun.fieldmeta.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;


import klg.common.dataaccess.BaseServiceImpl;
import klg.query.jpa.expr.AExpr;

import java.util.List;

import org.coderfun.fieldmeta.auth.AuthService;
import org.coderfun.fieldmeta.common.SystemCode;
import org.coderfun.fieldmeta.dao.ModuleDAO;
import org.coderfun.fieldmeta.entity.Module;
import org.coderfun.fieldmeta.entity.Module_;
import org.coderfun.fieldmeta.entity.Project;
import org.coderfun.fieldmeta.entity.Project_;

@Service
public class ModuleServiceImpl  extends BaseServiceImpl<Module, Long> implements ModuleService{
	@Autowired
	ModuleDAO moduleDAO;

	@Autowired
	AuthService authService;
	
	@Autowired
	ProjectService projectService;
	
	@Override
	public List<Module> getListByDefalutProject() {
		// TODO Auto-generated method stub

		
		return moduleDAO.findAll();
	}
}
