package org.coderfun.fieldmeta.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2018-12-14T01:22:09.438+0800")
@StaticMetamodel(Validation.class)
public class Validation_ {
	public static volatile SingularAttribute<Validation, String> code;
	public static volatile SingularAttribute<Validation, String> description;
	public static volatile SingularAttribute<Validation, String> javaValid;
	public static volatile SingularAttribute<Validation, String> jsValid;
	public static volatile SingularAttribute<Validation, String> message;
	public static volatile SingularAttribute<Validation, String> name;
	public static volatile SingularAttribute<Validation, Long> workspaceId;
}
