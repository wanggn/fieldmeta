package org.coderfun.fieldmeta.web.workspace;


import java.util.List;

import org.coderfun.fieldmeta.common.StringContainMatcher;
import org.coderfun.workspace.dict.entity.WPCodeClass;
import org.coderfun.workspace.dict.entity.WPCodeClass_;
import org.coderfun.workspace.dict.entity.WPCodeItem;
import org.coderfun.workspace.dict.service.WPCodeClassService;
import org.coderfun.workspace.interceptor.WorkspaceInject;
import org.coderfun.workspace.interceptor.WorkspaceValid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ch.qos.logback.core.boolex.Matcher;
import klg.common.model.EasyUIPage;
import klg.common.model.JsonData;
import klg.common.model.JsonData.Type;
import klg.query.jpa.expr.AExpr;


@Controller("adminWPCodeClassController")
@RequestMapping("/admin/action/codeclass")
public class WPCodeClassController {
	@Autowired
	WPCodeClassService codeClassService;
	
	@ResponseBody
	@RequestMapping("/add")
	public JsonData add(
			@ModelAttribute WPCodeClass codeClass){
		
		if(codeClassService.getOne(AExpr.eq(WPCodeClass_.code,codeClass.getCode()))==null)
			codeClassService.save(codeClass);
		else
			JsonData.success().setType(Type.error).setMessage("重复的代码！");
		return JsonData.success();
	}
	
	
	@ResponseBody
	@RequestMapping("/edit")
	public JsonData edit(
			@ModelAttribute WPCodeClass codeClass){
		
		codeClassService.update(codeClass);
		return JsonData.success();
	}
	
	@WorkspaceValid(entityClass = WPCodeClass.class)
	@ResponseBody
	@RequestMapping("/delete")
	public JsonData delete(
			@RequestParam Long id){
		
		codeClassService.delete(id);
		return JsonData.success();
	}
	
	@ResponseBody
	@RequestMapping("/findpage")
	public EasyUIPage findpage(
			@ModelAttribute WPCodeClass codeClass,
			@RequestParam int page,
			@RequestParam int rows){
		Pageable pageable=new PageRequest(page<1?0:page-1, rows, new Sort(Direction.DESC,"orderNum"));
		Page<WPCodeClass> pageData=codeClassService.findPage(codeClass, pageable);
		return new EasyUIPage(pageData);
	}
	
	@ResponseBody
	@RequestMapping("/findlist")
	public JsonData findlist(
			@ModelAttribute WPCodeClass codeClass){
		
		List<WPCodeClass> listData=codeClassService.findList(codeClass, new Sort(Direction.DESC,"orderNum"));
		return JsonData.success(listData);
	}
	
	@ResponseBody
	@RequestMapping("/datalist")
	public List datalist(
			@ModelAttribute WPCodeClass codeClass){
		
		ExampleMatcher matcher =StringContainMatcher.build("name");
		List<WPCodeClass> listData=codeClassService.findList(Example.of(codeClass,matcher), 
				new Sort(Direction.DESC,"orderNum"));
		
		return listData;
	}	
}

