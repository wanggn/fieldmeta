package org.coderfun.fieldmeta.web.router;

import org.coderfun.fieldmeta.auth.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WebFrontRouter {
	@Autowired
	AuthService authService;

	@RequestMapping(value = { "/index", "/", "" }, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("workspaceId", authService.getWorkspaceId());
		return "/index";
	}

	@RequestMapping("/login")
	public String login() {
		if (authService.getMember() != null) {// 已登录
			return "redirect:/";
		}
		return "/login";
	}
	
	
	@Value("${register.captcha.expire}")
	int captchaExpire;
	@RequestMapping("/register")
	public String register(Model model) {
		model.addAttribute("captchaExpire", captchaExpire);
		return "/register";
	}
	@RequestMapping("/resetPassword")
	public String resetPassword(Model model){
		model.addAttribute("captchaExpire", captchaExpire);
		return "/resetPassword";
	}
	
}
