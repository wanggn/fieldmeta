package org.coderfun.fieldmeta.web.workspace;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.coderfun.fieldmeta.common.StringContainMatcher;
import org.coderfun.fieldmeta.entity.EntityField;
import org.coderfun.fieldmeta.entity.PageField;
import org.coderfun.fieldmeta.entity.Tablemeta;
import org.coderfun.fieldmeta.service.TablemetaService;
import org.coderfun.workspace.interceptor.WorkspaceValid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import klg.common.model.EasyUIPage;
import klg.common.model.JsonData;

@Controller("adminTablemetaController")
@RequestMapping("/admin/action/tablemeta")
public class TablemetaController {
	@Autowired
	TablemetaService tablemetaService;

	@ResponseBody
	@RequestMapping("/add")
	public JsonData add(@ModelAttribute Tablemeta tablemeta) {

		tablemetaService.save(tablemeta);
		return JsonData.success();
	}

	@ResponseBody
	@RequestMapping("/save_fields")
	public JsonData saveFields(@RequestBody TableMetaModel model, @RequestParam String tableName) {

		List<EntityField> entityFields = model.entityFields;
		List<PageField> pageFields = model.pageFields;
		tablemetaService.saveFields(tableName, entityFields, pageFields);
		return JsonData.success();
	}

	@ResponseBody
	@RequestMapping("/edit")
	public JsonData edit(@ModelAttribute Tablemeta tablemeta) {

		tablemetaService.update(tablemeta);
		return JsonData.success();
	}

	@WorkspaceValid(entityClass = Tablemeta.class)
	@ResponseBody
	@RequestMapping("/delete")
	public JsonData delete(@RequestParam Long id) {

		tablemetaService.delete(id);
		return JsonData.success();
	}

	@ResponseBody
	@RequestMapping("/findlist")
	public JsonData findlist(@ModelAttribute Tablemeta tablemeta) {
		ExampleMatcher matcher =StringContainMatcher.build("tableName");
		List<Tablemeta> listData = tablemetaService.findList(Example.of(tablemeta,matcher), 
				new Sort(Direction.DESC, "id"));
		return JsonData.success(listData);
	}
}
class TableMetaModel {
	public List<EntityField> entityFields = new ArrayList<>();
	public List<PageField> pageFields = new ArrayList<>();
}