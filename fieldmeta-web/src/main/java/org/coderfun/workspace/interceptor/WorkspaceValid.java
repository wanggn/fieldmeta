package org.coderfun.workspace.interceptor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface WorkspaceValid {
	
	Class<?> entityClass();
	String entityAuthField()default "workspaceId";
	
	String entityIdValueArg() default "id";
}
