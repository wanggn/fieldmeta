package org.coderfun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@ImportResource(locations = { "classpath:app-jpa.xml" })
@SpringBootApplication(scanBasePackages = { "org.coderfun.*" })
public class Application {

	public static void main(String[] args) {

		SpringApplication.run(Application.class, args);
	}
}
