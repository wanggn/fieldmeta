<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/index.css"/>
	<c:import url="/admin/pages/common/headsource.jsp"/> 		

<title>fieldmeta桌面 </title>
</head>
<body class="easyui-layout" data-options="fit:true">
		<div data-options="region:'north'" style="height:70px" id="index-header"> 
		<span class="project-name">fieldmeta - 字段元数据</span>
		
		<span class="right-bar">
				<span>
				<label>工作空间：</label>
				<input name="workspaceId" id="workspaceId" value="${workspaceId}" class="easyui-combobox" style="width: 100px"
					data-options="">
				</span>			
			<a href="javascript:void(0)" onclick="javascript:logout()" class="easyui-linkbutton index_top_linkbutton" plain="true" icon="icon-bullet_go" >安全退出</a>
   		</span>		
		</div>
		<div data-options="region:'south',split:true" style="height:50px;">
		<c:import url="/WEB-INF/views/_loginFooter.jsp"/>
		</div>
		<div data-options="region:'west',split:true" title="菜单" style="width:220px;">
			<ul class="easyui-tree" data-options="url:'tree_menu.json',method:'get',animate:true,onClick:treeClick,onSelect:treeSelect"></ul>
		</div>
		<div data-options="region:'center',iconCls:'icon-ok'">
			<div class="easyui-tabs" id="tt" data-options="fit:true,border:false,plain:true,enableConextMenu:true,contextMenu:tab_menu">

			</div>
		</div>
</body>

<script src="http://www.easyui-extlib.com/Scripts/jquery-extensions/jquery.jdirk.js"></script>
<script src="http://www.easyui-extlib.com/Scripts/jquery-easyui-extensions/panel/jeasyui.extensions.panel.iframe.js"></script>
<script src="http://www.easyui-extlib.com/Scripts/jquery-easyui-extensions/menu/jeasyui.extensions.menu.js"></script>
<script src="http://www.easyui-extlib.com/Scripts/jquery-easyui-extensions/tabs/jeasyui.extensions.tabs.getTabs.js"></script>
<script src="http://www.easyui-extlib.com/Scripts/jquery-easyui-extensions/tabs/jeasyui.extensions.tabs.closeTabs.js"></script>
<script src="http://www.easyui-extlib.com/Scripts/jquery-easyui-extensions/tabs/jeasyui.extensions.tabs.contextMenu.js"></script>
<script>
		//tab右键刷新菜单
		var tab_menu=[{id:'refresh',text:'刷新',hideOnClick:true,handler:function(e, menuItem, menu, target, title, index){
			 var tab = $(target).tabs('getSelected');  // 获取选择的面板
			 tab.panel('refresh');
		}}]

		// 打开一个tab iframe
		function addTab(option){
			if ($('#tt').tabs('exists', option.title)){
				$('#tt').tabs('select', option.title);
			} else {
				$('#tt').tabs('add',option);
			}
		}
		//tree 的点击事件，新打开一个tab iframe
		function treeClick(node)
		{
			var title = node.text;	
			if(node.url){
				var url = node.url;
				var icon= node.iconCls;

				if(url.charAt(0) == '/')
					url = "${pageContext.request.contextPath}" + url;
				else
					url =  url;	
				addTab({
					title:title,
					closable:true,
					href:url,
					iniframe:true
				});		
			}
		}
		//tree的 select事件，打开或关闭节点的触发器，target参数是一个节点DOM对象。
		function treeSelect(node){
			$(this).tree('toggle',node.target);   
		}
		
		
		function logout(){
		    $.messager.confirm('提示','确定要退出?',function(r){
		        if (r){
		        	window.location.href="${root}/member/logout";
		        }
		    });
			
		}
		
		$("#workspaceId").combobox({
			url : adminActionPath + '/workspace/findlist',
			valueField:'id',
			textField:'name',
			editable:false,
			panelHeight:'auto',
			loadFilter : function(json){
				return json.data;
			}
			
		})
		
		
</script>
</html>